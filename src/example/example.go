package example

import "fmt"

// public struct, accessible outside the package
type Example struct {
	test string
}

// Example initializer function.
// This is a package level function
// Similar to a public static method in other languages
func New(exampleString string) *Example {
	return &Example{
		test: exampleString,
	}
}

// This is a public instance function
// The capital camel case makes this public.
func (example *Example) Print() {
	example.print()
}

// This is a private instance function
// The lower camel case makes this private.
func (example *Example) print() {
	fmt.Print(example.test)
}
