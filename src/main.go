package main

import "gitlab.com/gatesumdotnet/go-modules-example/example"

func main() {
	// short hand var assignment
	// same as var exampleInstance = example.New()
	exampleInstance := example.New("Hello World")
	// Accessing public instance function
	exampleInstance.Print()
	// exampleInstance.print() is unavailable outside the example package.
}
