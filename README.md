# Initialization Steps
```shell script
mkdir -p GoModulesExample/src/vendor
cd GoModulesExample/
touch README.md
git init -q
git remote add origin git@gitlab.com:gatesumdotnet/go-modules-example.git
touch Makefile
touch .gitignore
cd src
go mod init gitlab.com/gatesumdotnet/go-modules-example
touch main.go
```

# Installing Go

## Windows
### Using Scoop
```shell script
scoop install go
```

## Linux (Debian/Ubuntu)
```shell script
sudo snap install go
```

## Mac
```shell script
brew install go
```

# Adding Go Vendor Dependencies
```
# Add import to main.go, or whichever go file you require the import.
import "gitlab.com/gatesumdotnet/go-modules-example/example"
```
```shell script
go build -o HelloWorld.(exe|bin)
./HellowWorld.(exe|bin)
```

# More Resources
[Go Modules Quick Start](https://github.com/golang/go/wiki/Modules#quick-start)